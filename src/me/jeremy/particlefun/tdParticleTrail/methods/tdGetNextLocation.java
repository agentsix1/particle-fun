package me.jeremy.particlefun.tdParticleTrail.methods;
import org.bukkit.Location;
import org.bukkit.util.Vector;


public class tdGetNextLocation {	
	/*
	 *  Determines the next location of which is closest to the end location by calculating the distance from the current to end locations
	 * 	
	 * @param current
	 * 		- The starting location
	 * @param end
	 * 		- The ending location
	 */
	
	public Location getNextLocation(Location current, Location end) {
		Vector target = end.toVector(); // Changes end into a vector
		current.setDirection(target.subtract(current.toVector())); //Sets the direction value for current
		Vector increase = current.getDirection().multiply(0.5); //Sets the distance of how far to make the next particle
		return current.add(increase); //Adds the distance above to the current location.
	}
	/*
	 * End of getNextLocation
	 * 
	 */
	
}
