package me.jeremy.particlefun.tdParticleTrail.methods;

import java.util.List;

import me.jeremy.particlefun.particleLine;

public class tdUpdateParticleQue {
	/*
	 * This is used to update the particleQue list.
	 * @param indexLine
	 *   - This gets the List<String> (the array of settings for that particle line)
	 * 
	 * @param indexValue
	 *   - This decides what value you would like to change in the specific list
	 *   
	 * @param value
	 *   - This will be the new value for the item you have chosen
	 * 
	 */
	public void updateParticleQue(Integer indexLine, Integer indexValue, String value) {
		List<String> que = particleLine.particleQue.get(indexLine);
		que.set(indexValue, value);
		particleLine.particleQue.set(indexLine, que);
	}
}
