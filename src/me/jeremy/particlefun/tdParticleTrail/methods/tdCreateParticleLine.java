package me.jeremy.particlefun.tdParticleTrail.methods;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.jeremy.particlefun.particleLine;

public class tdCreateParticleLine {
	
	public void createParticleLine(Integer ticksPerStep, Double spacing,
			Location start, Location end, List<Player> players, String particle, Float offsetX, Float offsetY,
			Float offsetZ, Integer speed, Integer amount) {
		
		//particleQue Array = {Integer step, Integer steps, Integer ticksPerStep, Integer tickClock, Double null, Double null, Double null, 
		// Location start, Location end, Location current, List<Player> players, EnumParticle particle, Float offsetX, Float offsetY, Float offsetZ, Integer speed, Integer amount}
		/*
		 * This creates a list array of settings for the particle trail you like to create
		 * 
		 * Side note: the spacing this is unused and is there cause im to lazy to remove it...
		 */
		Integer steps = (int) Math.round((start.distance(end)*2)); //Caculates how many steps
		String playerList = "";
		for (Player p : players) { //Creates a string list of player names you want to view the particle affect
			if (playerList == "") {
				playerList = p.getName();
			} else {
				playerList = p.getName() + ",";
			}
		}
		String startCords = start.getWorld().getName() + "," + start.getX() + "," + start.getY() + "," + start.getZ(); //Changing Location into String
		String endCords = end.getWorld().getName() + "," + end.getX() + "," + end.getY() + "," + end.getZ(); //Changing Location into String
		String currentCords = start.getWorld().getName() + "," + start.getX() + "," + start.getY() + "," + start.getZ(); //Changing Location into String
		
		
		//0, steps, ticksPerStep, 0, null, null, null, startCords, endCords, currentCords, playerList, particle, offsetX, offsetY, offsetZ, speed, amount
		/*
		 * Creating the actual array
		 */
		List<String> tempLine = new ArrayList<String>();
		tempLine.add(0 + "");
		tempLine.add((steps) + "");
		tempLine.add(ticksPerStep + "");
		tempLine.add(0 + "");
		tempLine.add("" + "");
		tempLine.add("" + "");
		tempLine.add("" + "");
		tempLine.add(startCords);
		tempLine.add(endCords);
		tempLine.add(currentCords);
		tempLine.add(playerList);
		tempLine.add(particle + "");
		tempLine.add(offsetX + "");
		tempLine.add(offsetY + "");
		tempLine.add(offsetZ + "");
		tempLine.add(speed + "");
		tempLine.add(amount + "");
		particleLine.particleQue.add(tempLine);  //Adding array to particleQue array.    
	}
}
