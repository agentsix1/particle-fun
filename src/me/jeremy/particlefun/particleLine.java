package me.jeremy.particlefun;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import me.jeremy.particlefun.tdParticleTrail.methods.tdCreateParticleLine;
import me.jeremy.particlefun.tdParticleTrail.methods.tdGetNextLocation;
import me.jeremy.particlefun.tdParticleTrail.methods.tdGetParticleType;
import me.jeremy.particlefun.tdParticleTrail.methods.tdUpdateParticleQue;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;

public class particleLine extends JavaPlugin {
	public static List<List<String>> particleQue = new ArrayList<List<String>>();
	tdGetParticleType getParticleType = new tdGetParticleType();
	tdGetNextLocation getNextLocation = new tdGetNextLocation();
	tdUpdateParticleQue updateParticleQue = new tdUpdateParticleQue();
	tdCreateParticleLine createParticleLine = new tdCreateParticleLine();
	Location starting;
	Location ending;
	@Override
	public void onEnable() {
		BukkitScheduler scheduler = getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
			/*
			 * Executes the particle trails that are in the particleQue
			 * 
			 */
			@Override
            public void run() {
				List<List<String>> tempPQ = particleQue;
				int i = 0;
					for (List<String> a : tempPQ) {
						/*
						 * Sets default values. Its a bit bulky but works.
						 */
						Integer step = Integer.parseInt(a.get(0));
						//Integer steps = Integer.parseInt(a.get(1));
						Integer ticksPerStep = Integer.parseInt(a.get(2));
						Integer tickClock = Integer.parseInt(a.get(3));
						//Double unused0 = Double.parseDouble(a.get(4));
						//Double unused1 = Double.parseDouble(a.get(5));
						//Double unused2 = Double.parseDouble(a.get(6));
						//Location start = new Location(Bukkit.getWorld(a.get(7).split(",")[0]), Double.parseDouble(a.get(7).split(",")[1]), Double.parseDouble(a.get(7).split(",")[2]), Double.parseDouble(a.get(7).split(",")[3]));
						Location end = new Location(Bukkit.getWorld(a.get(8).split(",")[0]), Double.parseDouble(a.get(8).split(",")[1]), Double.parseDouble(a.get(8).split(",")[2]), Double.parseDouble(a.get(8).split(",")[3]));
						Location current = new Location(Bukkit.getWorld(a.get(9).split(",")[0]), Double.parseDouble(a.get(9).split(",")[1]), Double.parseDouble(a.get(9).split(",")[2]), Double.parseDouble(a.get(9).split(",")[3]));
						EnumParticle particle = getParticleType.getEnumParticleType(a.get(11));
						List<Player> players = new ArrayList<Player>();
						for (String u : a.get(10).split(",")) {
							players.add(Bukkit.getPlayer(u));
						}
						Float offsetX = Float.parseFloat(a.get(12));
						Float offsetY = Float.parseFloat(a.get(13));
						Float offsetZ = Float.parseFloat(a.get(14));
						Integer speed = Integer.parseInt(a.get(15));
						Integer amount = Integer.parseInt(a.get(16));
						/*
						 * This checks the tick clock and determines if it needs to run the new particle
						 */
						if (tickClock+1 >= ticksPerStep) {
							
							Location next = getNextLocation.getNextLocation(current, end);
							/*
							 * This executes the particle
							 */
							PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(
					        		particle, true, (float) next.getX(), (float) next.getY(), (float) next.getZ(), (float) offsetX, (float) offsetY, (float) offsetZ, speed, amount);
					      	for (Player p : players) {
					      		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
					      	}
					      	/*
					      	 * This updates the particle line with new values
					      	 */
					      		Location tempCur = getNextLocation.getNextLocation(current, end);
						      	String curLoc = tempCur.getWorld().getName() + "," + tempCur.getX() + "," + tempCur.getY() + "," + tempCur.getZ();
						      	updateParticleQue.updateParticleQue(i, 9, curLoc);
						      	updateParticleQue.updateParticleQue(i, 0, (step+1) + "");
						      	updateParticleQue.updateParticleQue(i, 3, 0 + "");
					      	
						} else {
							/*
							 * This updates the tick clock if it did not run this tick
							 */
							updateParticleQue.updateParticleQue(i, 3, (tickClock + 1) + "");
							
						}

				      	i += 1;
					}
					i=0;
					/*
					 * This removes any completed particle lines
					 */
					try {
						for (List<String> a : tempPQ) {
							Integer step = Integer.parseInt(a.get(0));
							Integer steps = Integer.parseInt(a.get(1));
							if ((step+1) >= steps) {
								tempPQ.remove(i);
							}
							i+=1;
							if (tempPQ.size() == 0) {
								return;
							}
						}
					} catch (ConcurrentModificationException e) {
						
					}
					
				i=0;
		        
			}
        }, 0L, 1L);
	}
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			/*
			 * -/makeline start
			 * -/makeline end
			 * These commands set the starting and ending location of a test particle line
			 */
			if (cmdLabel.equalsIgnoreCase("makeline") && p.isOp()) {
				if (args.length == 1) {
					if (args[0].equalsIgnoreCase("start")) {
						starting = p.getLocation();
						return true;
					}
					if (args[0].equalsIgnoreCase("end")) {
						ending = p.getLocation();
						return true;
					}
				}
				/*
				 * -/makeline
				 * Executes the test particle line you have created.
				 */
				if (args.length==0) {
					List<Player> players = new ArrayList<Player>();
					players.add(p);
					Location b = starting;
					Location a = ending;
					createParticleLine.createParticleLine(1, 0.3, b, a, players, "REDSTONE", (float) -1, (float) 1, (float) 0, 1, 0);
					p.sendMessage("particle created");
				}
			}
		}
		return false;
		
	}
}
